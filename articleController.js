Article = require('./articleModal');
var multer = require('multer');


var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, './public');
    },
    filename: function (req, file, callback) {
      callback(null, file.fieldname + '-' + Date.now());
    }
  });
  


// handle index actions
exports.index = function(req,res){
    Article.get(function(err,articles){
        if(err){
            res.json({
                status:"error",
                message:err,
            })
        }else{
            res.json({
                status:"success",
                message:"article list recieved successfully",
                data:articles
            })
        }
    })
}


//handle Article Create APi
exports.new = function(req,res){
    var article = new Article();
    article.title = req.body.title;
    article.description = req.body.description;
    article.publishDate = new Date();
    //saving the article
    
    var upload = multer({ storage : storage}).single('image');
    var filePath = upload(req,res,function(err,filePath) {
        if(err) {
           return null
        }else{
            article.image = filePath;
            console.log('isArticle',article)
            var isArticle = saveArticle(article);
           
            if(!isArticle){
                res.json({
                    status:"error",
                    message:err,
                })
            }else{
                res.json({
                    status:"success",
                    message:"article saved successfully",
                    data:isArticle
                })
            }
        }
        
    });
   
    
}
saveArticle = function(article){
    
    article.save(function(err){
        if(err){
            return null;
        }else{
           return article;
        }
    })
}


//handle Update Article Api

exports.update = function(req,res){
    Article.findById(req.params.article_id,function(err,article){
        if(err){
            res.send(err);
        }else{
            article.title = req.body.title ? req.body.title : article.title;
            article.image = req.body.image;
            article.description = req.body.description;

            article.save(function(err){
                if(err){
                    res.json({
                        status:"error",
                        message:err,
                    })
                }else{
                    res.json({
                        status:"success",
                        message:"article updated successfully",
                        data:article
                    })
                }
            })
        }
    })
}
