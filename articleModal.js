var mongoose = require('mongoose');

//setup article Schema
var schemaValues = {
    title:{
        type:String,
        require:true
    },
    image:{
        type:String,
        require:true
    },
    description:{
        type:String,
        require:true
    },
    publishDate:{
        type:Date,
        require:Date.now
    }
};
var aticleSchema = mongoose.Schema(schemaValues);

//exporting Article Modal

var Article = module.exports = mongoose.model('article',aticleSchema);
module.exports.get = function(callback,limit){
    Article.find(callback).limit(limit);
}