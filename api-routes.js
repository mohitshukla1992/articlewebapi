// initilizing express
let router = require('express').Router();
//importing article controller
var articleController = require('./articleController');


// set default API 
router.get('/',function(req,res){
    res.json({
        status:"Api are working",
        message:"this is test api",
    })
});


// set end Points for Articles
router.route('/article')
    .get(articleController.index)
    .post(articleController.new);

router.route('/article/:article_id')
    .patch(articleController.update)


module.exports = router;